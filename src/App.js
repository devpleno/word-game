import React, { Component } from 'react';
import './App.css';

class App extends Component {

  nivel1 = {
    words: [
      { word: 'ZAP', discovered: false },
      { word: 'PAZ', discovered: false },
      { word: 'PA', discovered: false }
    ],
    available: [
      { letter: 'P', used: false },
      { letter: 'A', used: false },
      { letter: 'Z', used: false }
    ]
  }

  nivel2 = {
    words: [
      { word: 'AMAR', discovered: false },
      { word: 'RAMA', discovered: false },
      { word: 'RAM', discovered: false },
      { word: 'MAR', discovered: false },
      { word: 'AR', discovered: false },
      { word: 'RA', discovered: false },
      { word: 'ARMA', discovered: false }
    ],
    available: [
      { letter: 'M', used: false },
      { letter: 'A', used: false },
      { letter: 'A', used: false },
      { letter: 'R', used: false }
    ]
  }

  state = {
    time: 60,
    gameState: 'stopped',
    terminou: false,
    word: '',
    words: [],
    available: [],
    nivel: 1
  }

  renderWord(word, i) {
    if (word.discovered) {
      const letters = word.word.split('');
      return <p key={i}>{letters.map((wAtual, wi) => <span key={wi} className='letter-empty'>{wAtual}</span>)}</p>
    }

    const letters = ''.padStart(word.word.length, '_').split('');

    return <p key={i}>{letters.map((lAtual, li) => <span key={li} className='letter-empty'> _ </span>)}</p>
  }

  defineNivel() {
    if (this.state.nivel == 1) {
      this.setState({
        words: this.nivel1.words,
        available: this.nivel1.available
      })
    }

    if (this.state.nivel == 2) {
      this.setState({
        words: this.nivel2.words,
        available: this.nivel2.available
      })
    }
  }

  random() {
    const availableCopy = [...this.state.available]
    const available = []

    const t = this.state.available.length
    for (var i = 0; i < t; i++) {
      const rand = parseInt(Math.random() * (t - i))
      available.push(availableCopy.splice(rand, 1)[0])
      this.setState({ available })
    }
  }

  renderAvailable(letter, i) {
    if (!letter.used) {
      return <span key={i} className='letter-available'>{letter.letter}</span>
    }

    return;
  }

  renderInterval() {

    const contagem = setInterval(() => {

      if (this.state.gameState == 'playing') {
        if (this.state.time % 10 == 0) {
          this.random();
        }

        this.setState({ time: this.state.time - 1 })
      }

      if (this.state.time == 0) {
        this.setState({ gameState: 'stopped' })
        clearInterval(contagem)
      }

    }, 1000)

  }

  componentDidMount() {

    this.defineNivel();

    this.renderInterval();

    window.addEventListener('keydown', evt => {

      const letter = evt.key.toUpperCase()

      if (letter === 'ENTER') {

        const word = this.state.word

        const wordIndex = this.state.words.findIndex(objWord => {
          return objWord.word === word && !objWord.discovered
        })

        if (wordIndex >= 0) {
          const words = [...this.state.words]
          words[wordIndex].discovered = true

          const available = this.state.available.map(w => {
            return { ...w, used: false }
          })

          this.setState({ available, words, word: '' })

          // Se não houver mais nenhuma palavra
          const index = this.state.words.findIndex(objLetter => {
            return !objLetter.discovered
          })

          if (index < 0) {
            this.setState({ gameState: 'stopped', nivel: 2, terminou: true })
            this.defineNivel();
          }
        }

      } else if (letter === 'BACKSPACE') {
        const len = this.state.word.length

        if (len > 0) {
          const l = this.state.word.substr(len - 1, len)
          const word = this.state.word.substr(0, len - 1)

          let index = -1

          this.state.available.forEach((lett, keyLett) => {
            if (lett.used && lett.letter === l) {
              index = keyLett
            }
          })

          const available = [...this.state.available]
          available[index].used = false

          this.setState({ available, word })
        }

      } else {

        const index = this.state.available.findIndex(objLetter => {
          return objLetter.letter === letter && !objLetter.used
        })

        if (index >= 0) {
          const available = [...this.state.available]
          available[index].used = true

          const word = this.state.word + letter
          this.setState({ available, word })
        }

      }

    })
  }

  componentWillUnmount() {
    window.removeEventListener('keydown')
  }

  render() {

    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Anagrama React</h1>
          <p>Tempo restante: {this.state.time}</p>
        </header>

        {
          (this.state.terminou && this.state.gameState == 'stopped') &&
          <div>PARABÉNS, você terminou este desafio</div>
        }

        {
          (this.state.gameState == 'stopped') &&
          <button onClick={() => {
            const words = this.state.words.map(w => {
              return { ...w, discovered: false }
            })

            this.setState({ gameState: 'playing', time: 60, words, terminou: false })
            this.renderInterval()
          }
          }>Iniciar Jogo</button>
        }

        {
          (this.state.time <= 0) &&
          <div>GAME OVER</div>
        }

        {
          (this.state.gameState == 'playing' && this.state.time > 0) &&
          <div>
            <div>
              {
                this.state.words.map((word, i) => this.renderWord(word, i))
              }
            </div>

            <hr />

            {
              (this.state.word.length > 0) &&
              <p className='word'>
                Letras digitadas: {this.state.word}
              </p>
            }

            <p className='word'>
              Letras disponiveis: {this.state.available.map((letter, i) => this.renderAvailable(letter, i))}
              <button onClick={() => this.random()}>Randow</button>
            </p>

          </div>
        }
      </div>
    );
  }
}

export default App;
